import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenusupirPage } from './menusupir.page';

const routes: Routes = [
  {
    path: '',
    component: MenusupirPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenusupirPageRoutingModule {}
