import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenusupirPage } from './menusupir.page';

describe('MenusupirPage', () => {
  let component: MenusupirPage;
  let fixture: ComponentFixture<MenusupirPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenusupirPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenusupirPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
