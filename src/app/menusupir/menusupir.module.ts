import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenusupirPageRoutingModule } from './menusupir-routing.module';

import { MenusupirPage } from './menusupir.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenusupirPageRoutingModule
  ],
  declarations: [MenusupirPage]
})
export class MenusupirPageModule {}
