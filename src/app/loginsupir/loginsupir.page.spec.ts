import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginsupirPage } from './loginsupir.page';

describe('LoginsupirPage', () => {
  let component: LoginsupirPage;
  let fixture: ComponentFixture<LoginsupirPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginsupirPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginsupirPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
