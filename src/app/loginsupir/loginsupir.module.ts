import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginsupirPageRoutingModule } from './loginsupir-routing.module';

import { LoginsupirPage } from './loginsupir.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginsupirPageRoutingModule
  ],
  declarations: [LoginsupirPage]
})
export class LoginsupirPageModule {}
