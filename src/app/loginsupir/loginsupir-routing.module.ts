import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginsupirPage } from './loginsupir.page';

const routes: Routes = [
  {
    path: '',
    component: LoginsupirPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginsupirPageRoutingModule {}
