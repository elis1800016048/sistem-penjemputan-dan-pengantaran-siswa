import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from '@angular/router';
import { AngularFirestore } from "@angular/fire/firestore";

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(
    private auth: AngularFireAuth,
    private router: Router,
    private db: AngularFirestore
  ) { }

  ngOnInit() {
  }

  data:any = {};
  registrasi() {
    this.auth.auth.createUserWithEmailAndPassword(this.data.email, this.data.password).then(res => {
      this.updateUser(res);
    }).catch(error => {
      console.log(error)
      alert('Error');
    })
  }

  updateUser(res) {
    var dt = {
      nis: this.data.nis,
      nama_siswa: this.data.nama_siswa,
      nama_orangtua: this.data.nama_orangtua,
      email: this.data.email,
      alamat: this.data.alamat,
      nomor_hp: this.data.nomor_hp,
    }
    this.db.collection('users').doc(res.user.uid).set(dt).then(res => {
      this.router.navigate(['home']);
    })
  }

}
