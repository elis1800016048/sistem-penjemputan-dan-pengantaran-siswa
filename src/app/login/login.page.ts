import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private auth: AngularFireAuth,
    private router: Router
  ) { }

  ngOnInit() {
  }

  email:any;
  password:any;
  login() {
    this.auth.auth.signInWithEmailAndPassword(this.email, this.password).then(res => {
      this.router.navigate(['menuutama']);
    }).catch(error => {
      alert('Email/Password Salah');
    })
  }

}
