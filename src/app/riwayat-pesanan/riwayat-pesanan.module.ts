import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RiwayatPesananPageRoutingModule } from './riwayat-pesanan-routing.module';

import { RiwayatPesananPage } from './riwayat-pesanan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RiwayatPesananPageRoutingModule
  ],
  declarations: [RiwayatPesananPage]
})
export class RiwayatPesananPageModule {}
