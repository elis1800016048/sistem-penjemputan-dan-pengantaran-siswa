import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PeriksaemailPageRoutingModule } from './periksaemail-routing.module';

import { PeriksaemailPage } from './periksaemail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PeriksaemailPageRoutingModule
  ],
  declarations: [PeriksaemailPage]
})
export class PeriksaemailPageModule {}
