import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PeriksaemailPage } from './periksaemail.page';

describe('PeriksaemailPage', () => {
  let component: PeriksaemailPage;
  let fixture: ComponentFixture<PeriksaemailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriksaemailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PeriksaemailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
