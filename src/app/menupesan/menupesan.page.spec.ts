import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenupesanPage } from './menupesan.page';

describe('MenupesanPage', () => {
  let component: MenupesanPage;
  let fixture: ComponentFixture<MenupesanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenupesanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenupesanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
