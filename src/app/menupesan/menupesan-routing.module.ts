import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenupesanPage } from './menupesan.page';

const routes: Routes = [
  {
    path: '',
    component: MenupesanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenupesanPageRoutingModule {}
