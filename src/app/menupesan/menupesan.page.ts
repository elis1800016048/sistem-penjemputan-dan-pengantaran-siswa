import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import * as L from "leaflet";


@Component({
  selector: 'app-menupesan',
  templateUrl: './menupesan.page.html',
  styleUrls: ['./menupesan.page.scss'],
})
export class MenupesanPage implements OnInit {

  map: L.Map;

  order: any = {};
  orderData: any = {};

  constructor(public router: Router,
      public auth: AngularFireAuth,
      public db: AngularFirestore) { }

  ngOnInit() {
    this.loadMap();
    this.auth.auth.onAuthStateChanged(res => {
        this.orderData = res;
    });
}

  loadMap() {
    this.map = L.map('map', {
      center: [-7.8068289, 110.3828864],
      zoom: 15,
      renderer: L.canvas()
  });

  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoib2treWJ1ZGRpIiwiYSI6ImNramt0bGNhYjVqYjIyem5wcjAyejVueXcifQ.uc47UPQjvcqxtb6Ab8aDSw', {
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1Ijoib2treWJ1ZGRpIiwiYSI6ImNramt0bGNhYjVqYjIyem5wcjAyejVueXcifQ.uc47UPQjvcqxtb6Ab8aDSw'
  }).addTo(this.map);

  
}

konfirmasi() {
  var dt = {
    dari:this.order.dari,
    ke:this.order.ke
  }
  console.log(dt)
  localStorage.setItem('datapesanan', JSON.stringify(dt));
  this.router.navigate(['/menubayar']);
}

}