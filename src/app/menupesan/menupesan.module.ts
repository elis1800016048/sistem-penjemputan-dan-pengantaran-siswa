import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenupesanPageRoutingModule } from './menupesan-routing.module';

import { MenupesanPage } from './menupesan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenupesanPageRoutingModule
  ],
  declarations: [MenupesanPage]
})
export class MenupesanPageModule {}
