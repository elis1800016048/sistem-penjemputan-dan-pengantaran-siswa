import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CekjadwalPage } from './cekjadwal.page';

describe('CekjadwalPage', () => {
  let component: CekjadwalPage;
  let fixture: ComponentFixture<CekjadwalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CekjadwalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CekjadwalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
