import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CekjadwalPage } from './cekjadwal.page';

const routes: Routes = [
  {
    path: '',
    component: CekjadwalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CekjadwalPageRoutingModule {}
