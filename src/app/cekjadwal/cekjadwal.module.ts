import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CekjadwalPageRoutingModule } from './cekjadwal-routing.module';

import { CekjadwalPage } from './cekjadwal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CekjadwalPageRoutingModule
  ],
  declarations: [CekjadwalPage]
})
export class CekjadwalPageModule {}
