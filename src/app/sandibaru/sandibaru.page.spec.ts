import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SandibaruPage } from './sandibaru.page';

describe('SandibaruPage', () => {
  let component: SandibaruPage;
  let fixture: ComponentFixture<SandibaruPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SandibaruPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SandibaruPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
