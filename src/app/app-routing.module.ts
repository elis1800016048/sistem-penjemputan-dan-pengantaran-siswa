import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'awal',
    pathMatch: 'full'
  },
  {
    path: 'awal',
    loadChildren: () => import('./awal/awal.module').then( m => m.AwalPageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },  {
    path: 'profile-user',
    loadChildren: () => import('./profile-user/profile-user.module').then( m => m.ProfileUserPageModule)
  },
  {
    path: 'detail-supir',
    loadChildren: () => import('./detail-supir/detail-supir.module').then( m => m.DetailSupirPageModule)
  },
  {
    path: 'riwayat-pesanan',
    loadChildren: () => import('./riwayat-pesanan/riwayat-pesanan.module').then( m => m.RiwayatPesananPageModule)
  },
  {
    path: 'loginsupir',
    loadChildren: () => import('./loginsupir/loginsupir.module').then( m => m.LoginsupirPageModule)
  },
  {
    path: 'menuutama',
    loadChildren: () => import('./menuutama/menuutama.module').then( m => m.MenuutamaPageModule)
  },
  {
    path: 'resetsandi',
    loadChildren: () => import('./resetsandi/resetsandi.module').then( m => m.ResetsandiPageModule)
  },
  {
    path: 'periksaemail',
    loadChildren: () => import('./periksaemail/periksaemail.module').then( m => m.PeriksaemailPageModule)
  },
  {
    path: 'sandibaru',
    loadChildren: () => import('./sandibaru/sandibaru.module').then( m => m.SandibaruPageModule)
  },
  {
    path: 'menupesan',
    loadChildren: () => import('./menupesan/menupesan.module').then( m => m.MenupesanPageModule)
  },
  {
    path: 'cekjadwal',
    loadChildren: () => import('./cekjadwal/cekjadwal.module').then( m => m.CekjadwalPageModule)
  },
  {
    path: 'menubayar',
    loadChildren: () => import('./menubayar/menubayar.module').then( m => m.MenubayarPageModule)
  },
  {
    path: 'menusupir',
    loadChildren: () => import('./menusupir/menusupir.module').then( m => m.MenusupirPageModule)
  },
  {
    path: 'pesanan-supir',
    loadChildren: () => import('./pesanan-supir/pesanan-supir.module').then( m => m.PesananSupirPageModule)
  },
  {
    path: 'hasil-pesanan',
    loadChildren: () => import('./hasil-pesanan/hasil-pesanan.module').then( m => m.HasilPesananPageModule)
  },
  {
    path: 'menunggu-supir',
    loadChildren: () => import('./menunggu-supir/menunggu-supir.module').then( m => m.MenungguSupirPageModule)
  },
  {
    path: 'detail-berangkat',
    loadChildren: () => import('./detail-berangkat/detail-berangkat.module').then( m => m.DetailBerangkatPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
