import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuutamaPageRoutingModule } from './menuutama-routing.module';

import { MenuutamaPage } from './menuutama.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuutamaPageRoutingModule
  ],
  declarations: [MenuutamaPage]
})
export class MenuutamaPageModule {}
