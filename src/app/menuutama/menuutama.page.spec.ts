import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuutamaPage } from './menuutama.page';

describe('MenuutamaPage', () => {
  let component: MenuutamaPage;
  let fixture: ComponentFixture<MenuutamaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuutamaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuutamaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
