import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenubayarPage } from './menubayar.page';

describe('MenubayarPage', () => {
  let component: MenubayarPage;
  let fixture: ComponentFixture<MenubayarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenubayarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenubayarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
