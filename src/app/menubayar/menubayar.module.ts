import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenubayarPageRoutingModule } from './menubayar-routing.module';

import { MenubayarPage } from './menubayar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenubayarPageRoutingModule
  ],
  declarations: [MenubayarPage]
})
export class MenubayarPageModule {}
