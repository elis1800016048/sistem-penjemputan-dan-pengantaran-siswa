import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenubayarPage } from './menubayar.page';

const routes: Routes = [
  {
    path: '',
    component: MenubayarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenubayarPageRoutingModule {}
