import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menubayar',
  templateUrl: './menubayar.page.html',
  styleUrls: ['./menubayar.page.scss'],
})
export class MenubayarPage implements OnInit {

  constructor(
    private db: AngularFirestore,
    private router: Router
  ) {}

  dataPesanan:any = {};
  ngOnInit() {
    this.dataPesanan = JSON.parse(localStorage.getItem('datapesanan'));
    console.log(this.dataPesanan)
  }

  pesan() {
    var idOrder = new Date().getTime()+''+Math.floor(Math.random()*10000);
    this.db.collection('orders').doc(idOrder).set(this.dataPesanan).then(res => {
      alert('Berhasil dipesan');
      this.router.navigate(['/menunggu-supir']);
    });
  }

}
