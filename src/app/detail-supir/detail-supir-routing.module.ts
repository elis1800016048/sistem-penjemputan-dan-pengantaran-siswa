import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailSupirPage } from './detail-supir.page';

const routes: Routes = [
  {
    path: '',
    component: DetailSupirPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailSupirPageRoutingModule {}
