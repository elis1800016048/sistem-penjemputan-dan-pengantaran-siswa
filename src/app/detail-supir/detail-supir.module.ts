import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailSupirPageRoutingModule } from './detail-supir-routing.module';

import { DetailSupirPage } from './detail-supir.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailSupirPageRoutingModule
  ],
  declarations: [DetailSupirPage]
})
export class DetailSupirPageModule {}
