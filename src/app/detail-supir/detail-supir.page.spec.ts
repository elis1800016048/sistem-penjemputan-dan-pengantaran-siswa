import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailSupirPage } from './detail-supir.page';

describe('DetailSupirPage', () => {
  let component: DetailSupirPage;
  let fixture: ComponentFixture<DetailSupirPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailSupirPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailSupirPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
