import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResetsandiPageRoutingModule } from './resetsandi-routing.module';

import { ResetsandiPage } from './resetsandi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResetsandiPageRoutingModule
  ],
  declarations: [ResetsandiPage]
})
export class ResetsandiPageModule {}
