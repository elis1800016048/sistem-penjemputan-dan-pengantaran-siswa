import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResetsandiPage } from './resetsandi.page';

describe('ResetsandiPage', () => {
  let component: ResetsandiPage;
  let fixture: ComponentFixture<ResetsandiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetsandiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResetsandiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
