import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-awal',
  templateUrl: './awal.page.html',
  styleUrls: ['./awal.page.scss'],
})
export class AwalPage implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  lanjut() {
    this.router.navigate(['login']);
  }

}
